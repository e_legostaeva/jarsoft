import React from 'react';

import {Nav, Navbar} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function NavigationBar() {
    return (
        <Navbar bg="light" variant="light">
            <Link to={""} className="navbar-brand">
                Testovoe
            </Link>
            <Nav className="mr-auto">
                <Link to={"category"} className="nav-link">Category</Link>
                <Link to={"banner"} className="nav-link">Banner</Link>
                <Link to={"add_category"} className="nav-link">Add Category</Link>
                <Link to={"add_banner"} className="nav-link">Add Banner</Link>
            </Nav>
        </Navbar>
    );
}