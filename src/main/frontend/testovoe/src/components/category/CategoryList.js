import React, {Component} from 'react';

import '../Style.css';
import {Button, ButtonGroup, Card, FormControl, InputGroup, Table, Toast} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faList, faSearch, faTimes, faTrash} from "@fortawesome/free-solid-svg-icons";
import axios from 'axios';
import InfoToast from "../InfoToast";
import {Link} from 'react-router-dom';

export default class CategoryList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            categories: []
        };
    }

    componentDidMount() {
        this.findAllCategories();
    }

    findAllCategories() {
        axios.get("http://localhost:8080/category")
            .then(response => response.data)
            .then((data) => {
                this.setState({categories: data});
            });
    };

    deleteCategory = (categoryId) => {
        axios.delete("http://localhost:8080/category/" + categoryId)
            .then(response => {
                if (response.data != null) {
                    this.setState({"show": true});
                    setTimeout(() => this.setState({"show": false}), 3000);
                    this.setState({
                        categories: this.state.categories.filter(category => category.categoryId !== categoryId)
                    });
                } else {
                    this.setState({"show": false});
                }
            }).catch((error) => {
            this.setState({"showErrorToast": true, "errorToastInfo": error.response.data.message});
        });
    };

    searchChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    cancelSearch = () => {
        this.setState({"search": ''});
        this.findAllCategories();
    };

    searchData = () => {
        axios.get("http://localhost:8080/category/search/" + this.state.search)
            .then(response => response.data)
            .then((data) => {
                this.setState({categories: data});
            });
    };

    render() {
        const {categories, search} = this.state;

        const toastCss = {
            position: 'fixed',
            top: '10px',
            right: '10px',
            zIndex: '5'
        };

        return (
            <div>
                <div style={{"display": this.state.show ? "block" : "none"}}>
                    <InfoToast
                        show={this.state.show} message={"Category Deleted Successfully."} type={"success"}/>
                </div>
                <div style={{"display": this.state.showErrorToast ? "block" : "none"}}>
                    <div style={toastCss}>
                        <Toast onClose={() => this.setState({"showErrorToast": false})}>
                            <Toast.Header>
                                <strong className="mr-auto">Request failed</strong>
                            </Toast.Header>
                            <Toast.Body>{this.state.errorToastInfo}</Toast.Body>
                        </Toast>
                    </div>
                </div>
                <Card>
                    <Card.Header>
                        <div style={{"float": "left"}}>
                            <FontAwesomeIcon icon={faList}/> Categories
                        </div>
                        <div style={{"float": "right"}}>
                            <InputGroup size="sm">
                                <FormControl placeholder="Enter category name..." name="search" value={search}
                                             className={"info-border"}
                                             onChange={this.searchChange}/>
                                <InputGroup.Append>
                                    <Button size="sm" variant="outline-info" type="button" onClick={this.searchData}>
                                        <FontAwesomeIcon icon={faSearch}/>
                                    </Button>
                                    <Button size="sm" variant="outline-danger" type="button"
                                            onClick={this.cancelSearch}>
                                        <FontAwesomeIcon icon={faTimes}/>
                                    </Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </Card.Header>
                    <Card.Body>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                categories.length === 0 ?
                                    <tr align="center">
                                        <td colSpan="6">No Categories available</td>
                                    </tr> :
                                    categories.map((category) => (
                                        <tr key={category.categoryId}>
                                            <td>{category.categoryId}</td>
                                            <td>{category.categoryName}</td>
                                            <td>
                                                <ButtonGroup>
                                                    <Link to={"edit_category/" + category.categoryId}
                                                          className="btn btn-sm btn-outline-primary"><FontAwesomeIcon
                                                        icon={faEdit}/></Link>{' '}
                                                    <Button size="sm" variant="outline-danger"
                                                            onClick={this.deleteCategory.bind(this, category.categoryId)}><FontAwesomeIcon
                                                        icon={faTrash}/></Button>{' '}
                                                </ButtonGroup>
                                            </td>
                                        </tr>
                                    ))
                            }
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}