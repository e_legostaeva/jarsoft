import React, {Component} from 'react';

import {Button, Card, Col, Form} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faList, faPlusSquare, faSave, faUndo} from "@fortawesome/free-solid-svg-icons";
import InfoToast from '../InfoToast';
import axios from 'axios';

export default class Category extends Component {
    constructor(props) {
        super(props);
        this.state = this.initialState;
        this.state.show = false;
        this.categoryChange = this.categoryChange.bind(this);
        this.submitCategory = this.submitCategory.bind(this);
    }

    initialState = {
        id: '', categoryName: '', categoryReqName: ''
    };

    componentDidMount() {
        const categoryId = this.props.match.params.id;
        if (categoryId) {
            this.findCategoryById(categoryId);
        }
    }

    findCategoryById = (categoryId) => {
        axios.get("http://localhost:8080/category/" + categoryId)
            .then(response => {
                if (response.data != null) {
                    this.setState({
                        id: response.data.categoryId,
                        categoryName: response.data.categoryName,
                        categoryReqName: response.data.categoryReqName
                    });
                }
            }).catch((error) => {
            console.error("Error - " + error);
        });
    };

    updateCategory = event => {
        event.preventDefault();

        const category = {
            categoryId: this.state.id,
            categoryName: this.state.categoryName,
            categoryReqName: this.state.categoryReqName
        };

        axios.put("http://localhost:8080/category", category)
            .then(response => {
                if (response.data != null) {
                    this.setState({"show": true, "method": "put"});
                    setTimeout(() => this.setState({"show": false}), 3000);
                    setTimeout(() => this.categoryList(), 3000);
                } else {
                    this.setState({"show": false});
                }
            }).catch((error) => {
            this.setState({"showValidName": true, "validNameInfo": error.response.data.message});
            setTimeout(() => this.setState({"showValidName": false}), 6000);
        });
    };

    categoryChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    resetCategory = () => {
        this.setState(() => this.initialState);
    };

    submitCategory = event => {
        event.preventDefault();
        const category = {
            categoryName: this.state.categoryName,
            categoryReqName: this.state.categoryReqName
        };

        axios.post("http://localhost:8080/category", category)
            .then(response => {
                if (response.data != null) {
                    this.setState({"show": true, "method": "post"});

                    setTimeout(() => this.setState({"show": false}), 3000);
                } else {
                    this.setState({"show": false});
                }
            }).catch((error) => {
            this.setState({"showValidName": true, "validNameInfo": error.response.data.message});
            setTimeout(() => this.setState({"showValidName": false}), 6000);
        });
    };

    categoryList = () => {
        return this.props.history.push("/category");
    };

    render() {
        const {categoryName, categoryReqName} = this.state;
        return (
            <div>
                <div style={{"display": this.state.show ? "block" : "none"}}>
                    <InfoToast show={this.state.show}
                               message={this.state.method === "put" ? "Category Updated Successfully." : "Category Saved Successfully."}
                               type={"success"}/>
                </div>
                <div style={{"display": this.state.showValidName ? "block" : "none"}}>
                    <InfoToast
                        show={this.state.showValidName} message={this.state.validNameInfo} type={"danger"}/>
                </div>
                <Card>
                    <Card.Header>
                        <FontAwesomeIcon
                            icon={this.state.id ? faEdit : faPlusSquare}/>{this.state.id ? " Update Category" : " Save Category"}
                    </Card.Header>
                    <Form onReset={this.resetCategory}
                          onSubmit={this.state.id ? this.updateCategory : this.submitCategory} id="addCategoryId">
                        <Card.Body>
                            <Form.Group as={Col} controlId="formGridName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control required autoComplete="off"
                                              type="test"
                                              name="categoryName"
                                              value={categoryName}
                                              onChange={this.categoryChange}
                                              placeholder="Enter category name"/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formGridReqName">
                                <Form.Label>ReqName</Form.Label>
                                <Form.Control required autoComplete="off"
                                              type="test"
                                              name="categoryReqName"
                                              value={categoryReqName}
                                              onChange={this.categoryChange}
                                              placeholder="Enter category request name"/>
                            </Form.Group>

                        </Card.Body>
                        <Card.Footer style={{"textAlign": "right"}}>
                            <Button size="sm" variant="success" type="submit">
                                <FontAwesomeIcon icon={faSave}/> {this.state.id ? "Update" : "Save"}
                            </Button>{' '}
                            <Button size="sm" variant="info" type="reset">
                                <FontAwesomeIcon icon={faUndo}/> Reset
                            </Button>{' '}
                            <Button size="sm" variant="info" type="button" onClick={this.categoryList.bind()}>
                                <FontAwesomeIcon icon={faList}/> CategoryList
                            </Button>
                        </Card.Footer>
                    </Form>
                </Card>
            </div>
        );
    }
}