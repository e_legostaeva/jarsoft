import React, {Component} from 'react';

import '../Style.css';
import axios from 'axios';
import {Jumbotron} from "react-bootstrap";
import queryString from 'query-string';

export default class BannerCard extends Component {
    constructor(props) {
        super(props);
        this.state = this.initialState;
    }

    initialState = {
        bannerContent: '', categoryReqName: ''
    };

    componentDidMount() {
        const values = queryString.parse(this.props.location.search);
        this.initialState.categoryReqName = values['category'];
        this.findAvailableBanner(values['category'])
    }

    findAvailableBanner = (categoryReqName) => {
        axios.get("http://localhost:8080/bid?category=" + categoryReqName)
            .then((response) => {
                if (response.status === 204) {
                    this.setState({bannerContent: "No available banners."});
                } else {
                    this.setState({bannerContent: response.data.bannerContent});
                }
            });
    };

    render() {
        return (
            <Jumbotron>
                <p>{this.state.bannerContent}</p>
            </Jumbotron>
        );
    }
}