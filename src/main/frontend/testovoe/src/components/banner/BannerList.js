import React, {Component} from 'react';

import {Button, ButtonGroup, Card, FormControl, InputGroup, Table} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faList, faSearch, faTimes, faTrash} from "@fortawesome/free-solid-svg-icons";
import axios from 'axios';
import InfoToast from "../InfoToast";
import {Link} from 'react-router-dom';

export default class BannerList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            banners: []
        };
    }

    componentDidMount() {
        this.findAllBanners();
    }

    findAllBanners() {
        axios.get("http://localhost:8080/banner")
            .then(response => response.data)
            .then((data) => {
                this.setState({banners: data});
            });
    }

    deleteBanner = (bannerId) => {
        axios.delete("http://localhost:8080/banner/" + bannerId)
            .then(response => {
                if (response.data != null) {
                    this.setState({"show": true});
                    setTimeout(() => this.setState({"show": false}), 3000);
                    this.setState({
                        banners: this.state.banners.filter(banner => banner.bannerId !== bannerId)
                    });
                } else {
                    this.setState({"show": false});
                }
            });
    };

    searchChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    cancelSearch = () => {
        this.setState({"search": ''});
        this.findAllBanners();
    };

    searchData = () => {
        axios.get("http://localhost:8080/banner/search/" + this.state.search)
            .then(response => response.data)
            .then((data) => {
                this.setState({banners: data});
            });
    };

    render() {
        const {banners, search} = this.state;

        return (
            <div>
                <div style={{"display": this.state.show ? "block" : "none"}}>
                    <InfoToast
                        show={this.state.show} message={"Banner Deleted Successfully."} type={"danger"}/>
                </div>
                <Card>
                    <Card.Header>
                        <div style={{"float": "left"}}>
                            <FontAwesomeIcon icon={faList}/> Banners
                        </div>
                        <div style={{"float": "right"}}>
                            <InputGroup size="sm">
                                <FormControl placeholder="Enter banner name..." name="search" value={search}
                                             className={"info-border"}
                                             onChange={this.searchChange}/>
                                <InputGroup.Append>
                                    <Button size="sm" variant="outline-info" type="button" onClick={this.searchData}>
                                        <FontAwesomeIcon icon={faSearch}/>
                                    </Button>
                                    <Button size="sm" variant="outline-danger" type="button"
                                            onClick={this.cancelSearch}>
                                        <FontAwesomeIcon icon={faTimes}/>
                                    </Button>
                                </InputGroup.Append>
                            </InputGroup>
                        </div>
                    </Card.Header>
                    <Card.Body>
                        <Table striped bordered hover>
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                banners.length === 0 ?
                                    <tr align="center">
                                        <td colSpan="6">No Banners available</td>
                                    </tr> :
                                    banners.map((banner) => (
                                        <tr key={banner.bannerId}>
                                            <td>{banner.bannerId}</td>
                                            <td>{banner.bannerName}</td>
                                            <td>
                                                <ButtonGroup>
                                                    <Link to={"edit_banner/" + banner.bannerId}
                                                          className="btn btn-sm btn-outline-primary"><FontAwesomeIcon
                                                        icon={faEdit}/></Link>{' '}
                                                    <Button size="sm" variant="outline-danger"
                                                            onClick={this.deleteBanner.bind(this, banner.bannerId)}><FontAwesomeIcon
                                                        icon={faTrash}/></Button>{' '}
                                                </ButtonGroup>
                                            </td>
                                        </tr>
                                    ))
                            }
                            </tbody>
                        </Table>
                    </Card.Body>
                </Card>
            </div>
        );
    }
}