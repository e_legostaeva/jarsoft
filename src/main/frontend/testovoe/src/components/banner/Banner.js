import React, {Component} from 'react';

import {Button, Card, Col, Form} from 'react-bootstrap';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faList, faPlusSquare, faSave, faUndo} from "@fortawesome/free-solid-svg-icons";
import InfoToast from '../InfoToast';
import axios from 'axios';

export default class Banner extends Component {
    constructor(props) {
        super(props);
        this.state = this.initialState;
        this.state = {
            categories: [],
            show: false
        };
        this.bannerChange = this.bannerChange.bind(this);
        this.submitBanner = this.submitBanner.bind(this);
    }

    initialState = {
        id: '', bannerName: '', bannerPrice: '', categoryId: '', bannerContent: ''
    };

    componentDidMount() {
        console.log("componentDidMount " + this.props);
        const bannerId = this.props.match.params.id;
        if (bannerId) {
            this.findBannerById(bannerId);
        }
        this.findAllCategories();
    };

    findAllCategories = () => {
        axios.get("http://localhost:8080/category")
            .then(response => response.data)
            .then((data) => {
                this.setState({
                    categories: [{value: '', display: 'Select category'}]
                        .concat(data.map(category => {
                            return {value: category.categoryId, display: category.categoryName}
                        }))
                })
            });
    };

    findBannerById = (bannerId) => {
        axios.get("http://localhost:8080/banner/" + bannerId)
            .then(response => {
                if (response.data != null) {
                    this.setState({
                        id: response.data.bannerId,
                        bannerName: response.data.bannerName,
                        bannerPrice: response.data.bannerPrice,
                        categoryId: response.data.categoryDto.categoryId,
                        bannerContent: response.data.bannerContent
                    });
                }
            })
    };

    updateBanner = event => {
        event.preventDefault();

        const banner = {
            bannerId: this.state.id,
            bannerName: this.state.bannerName,
            bannerPrice: this.state.bannerPrice,
            categoryId: this.state.categoryId,
            bannerContent: this.state.bannerContent
        };

        axios.put("http://localhost:8080/banner", banner)
            .then(response => {
                if (response.data != null) {
                    this.setState({"show": true, "method": "put"});
                    setTimeout(() => this.setState({"show": false}), 3000);
                    setTimeout(() => this.bannerList(), 3000);
                } else {
                    this.setState({"show": false});
                }
            }).catch((error) => {
            this.setState({"showValidName": true, "validNameInfo": error.response.data.message});
            setTimeout(() => this.setState({"showValidName": false}), 6000);
        });
    };

    bannerChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    resetBanner = () => {
        this.setState(() => this.initialState);
    };

    submitBanner = event => {
        event.preventDefault();

        const banner = {
            bannerName: this.state.bannerName,
            bannerPrice: this.state.bannerPrice,
            categoryId: this.state.categoryId,
            bannerContent: this.state.bannerContent
        };

        axios.post("http://localhost:8080/banner", banner)
            .then(response => {
                if (response.data != null) {
                    this.setState({"show": true, "method": "post"});
                    setTimeout(() => this.setState({"show": false}), 3000);
                    setTimeout(() => this.bannerList(), 3000);
                } else {
                    this.setState({"show": false});
                }
            }).catch((error) => {
            this.setState({"showValidName": true, "validNameInfo": error.response.data.message});
            setTimeout(() => this.setState({"showValidName": false}), 6000);
        });
    };

    bannerList = () => {
        return this.props.history.push("/banner");
    };

    render() {
        const {bannerName, bannerPrice, categoryId, bannerContent} = this.state;

        return (
            <div>
                <div style={{"display": this.state.show ? "block" : "none"}}>
                    <InfoToast show={this.state.show}
                               message={this.state.method === "put" ? "Banner Updated Successfully." : "Banner Saved Successfully."}
                               type={"success"}/>
                </div>
                <div style={{"display": this.state.showValidName ? "block" : "none"}}>
                    <InfoToast
                        show={this.state.showValidName} message={this.state.validNameInfo} type={"danger"}/>
                </div>
                <Card>
                    <Card.Header>
                        <FontAwesomeIcon
                            icon={this.state.id ? faEdit : faPlusSquare}/>{this.state.id ? " Update Banner" : " Save Banner"}
                    </Card.Header>
                    <Form onReset={this.resetBanner}
                          onSubmit={this.state.id ? this.updateBanner : this.submitBanner} id="addBannerId">
                        <Card.Body>
                            <Form.Group as={Col} controlId="formGridName">
                                <Form.Label>Name</Form.Label>
                                <Form.Control required autoComplete="off"
                                              type="test"
                                              name="bannerName"
                                              value={bannerName}
                                              onChange={this.bannerChange}
                                              placeholder="Enter banner name"/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formBannerPrice">
                                <Form.Label>Price</Form.Label>
                                <Form.Control required autoComplete="off"
                                              type="number" step={0.01}
                                              name="bannerPrice"
                                              value={bannerPrice}
                                              onChange={this.bannerChange}
                                              placeholder="Enter banner price"/>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formBannerCategory">
                                <Form.Label>Category</Form.Label>
                                <Form.Control required as="select"
                                              custom onChange={this.bannerChange}
                                              name="categoryId" value={categoryId}>
                                    {this.state.categories.map(categoryId =>
                                        <option key={categoryId.value} value={categoryId.value}>
                                            {categoryId.display}
                                        </option>
                                    )}
                                </Form.Control>
                            </Form.Group>

                            <Form.Group as={Col} controlId="formBannerContent">
                                <Form.Label>Content</Form.Label>
                                <Form.Control required autoComplete="off"
                                              type="test"
                                              name="bannerContent"
                                              value={bannerContent}
                                              onChange={this.bannerChange}
                                              placeholder="Enter banner content"
                                              as="textarea" rows="3"/>
                            </Form.Group>

                        </Card.Body>
                        <Card.Footer style={{"textAlign": "right"}}>
                            <Button size="sm" variant="success" type="submit">
                                <FontAwesomeIcon icon={faSave}/> {this.state.id ? "Update" : "Save"}
                            </Button>{' '}
                            <Button size="sm" variant="info" type="reset">
                                <FontAwesomeIcon icon={faUndo}/> Reset
                            </Button>{' '}
                            <Button size="sm" variant="info" type="button" onClick={this.bannerList.bind()}>
                                <FontAwesomeIcon icon={faList}/> BannerList
                            </Button>
                        </Card.Footer>
                    </Form>
                </Card>
            </div>
        )
            ;
    }
}