import React from 'react';
import {Jumbotron} from "react-bootstrap";

export default function Welcome(props) {
    return (
        <Jumbotron>
            <h1>{props.heading}</h1>
            <p>{props.desc}</p>
        </Jumbotron>
    );
}