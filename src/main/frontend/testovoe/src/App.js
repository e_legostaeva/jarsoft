import React from 'react';
import './App.css';

import {Col, Container, Row} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

import NavigationBar from './components/NavigationBar';
import Welcome from './components/Welcome';
import BannerList from "./components/banner/BannerList";
import CategoryList from "./components/category/CategoryList";
import Category from './components/category/Category';
import Banner from "./components/banner/Banner"
import BannerCard from './components/banner/BannerCard';

export default function App() {
    const heading = "Welcome to testovoe";
    const desc = "This is my task from the company 'JarSoft'. Enjoy!";

    return (
        <Router>
            <NavigationBar/>
            <Container>
                <Row>
                    <Col lg={12} className={"margin-top"}>
                        <Switch>
                            <Route path="/" exact component={() => <Welcome heading={heading} desc={desc}/>}/>
                            <Route path="/category" exact component={CategoryList}/>
                            <Route path="/banner" exact component={BannerList}/>
                            <Route path="/add_category" exact component={Category}/>
                            <Route path="/edit_category/:id" exact component={Category}/>
                            <Route path="/edit_banner/:id" exact component={Banner}/>
                            <Route path="/add_banner" exact component={Banner}/>
                            <Route path="/bid" exact component={BannerCard}/>
                        </Switch>
                    </Col>
                </Row>
            </Container>
        </Router>
    );
}
