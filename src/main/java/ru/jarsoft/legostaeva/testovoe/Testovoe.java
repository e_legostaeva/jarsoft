package ru.jarsoft.legostaeva.testovoe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Testovoe {
    public static void main(String[] args) {
        SpringApplication.run(Testovoe.class, args);
    }
}
