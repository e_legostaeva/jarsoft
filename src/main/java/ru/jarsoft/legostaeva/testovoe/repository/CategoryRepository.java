package ru.jarsoft.legostaeva.testovoe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.jarsoft.legostaeva.testovoe.model.entity.Category;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {
    Category findCategoryByCategoryId(Integer id);

    Collection<Category> findAllByCategoryIsDeleted(Boolean deleted);

    Collection<Category> findAllByCategoryNameLikeAndCategoryIsDeletedFalse(@Param("searchText") String searchText);

    Optional<Category> findCategoryByCategoryReqNameAndCategoryIsDeletedFalse(String categoryReqName);

    @Query(value = "select category.category_id\n" +
            "from category\n" +
            "where UPPER(category.name) = UPPER(:name) and category.deleted = false",
            nativeQuery = true)
    Integer findCategoryIdWithNameLike(@Param("name") String name);

    @Query(value = "select category.category_id\n" +
            "from category\n" +
            "where UPPER(category.req_name) = UPPER(:name) and category.deleted = false",
            nativeQuery = true)
    Integer findCategoryIdWithReqNameLike(@Param("name") String name);
}
