package ru.jarsoft.legostaeva.testovoe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.jarsoft.legostaeva.testovoe.model.entity.Banner;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;

@Repository
public interface BannerRepository extends JpaRepository<Banner, Integer> {
    Collection<Banner> findAllByBannerIsDeleted(Boolean isDeleted);

    Banner findBannerByBannerId(Integer id);

    Collection<Banner> findAllByBannerNameLikeAndBannerIsDeletedFalse(@Param("searchText") String searchText);

    @Query(value = "select banner.banner_id\n" +
            "from banner\n" +
            "where UPPER(banner.name) = UPPER(:name) and banner.deleted = false",
            nativeQuery = true)
    Integer findBannerIdWithNameLike(@Param("name") String name);

    @Query(value = "select banner.banner_id\n" +
            "from banner\n" +
            "left join request r on banner.banner_id = r.banner_id\n" +
            "where banner.category_id = :category_id and banner.deleted = false\n" +
            "and not(ip_address <=> :ip_address)\n" +
            "and not(user_agent <=> :user_agent)\n" +
            "and not(DATEDIFF(:currentDateTime, date) <=> 0)",
            nativeQuery = true
    )
    List<Integer> findAllAvailableBanners(
            @Param("category_id") Integer categoryId,
            @Param("ip_address") String ipAddress,
            @Param("user_agent") String userAgent,
            @Param("currentDateTime") LocalDateTime currentDateTime
    );

    @Query(value = "select banner.banner_id\n" +
            "from banner\n" +
            "where banner.price = (select MAX(banner.price)\n" +
            "                      from banner\n" +
            "                      where banner.category_id = :category_id\n" +
            "                        and banner.deleted = false)\n" +
            "  and banner.deleted = false",
            nativeQuery = true)
    List<Integer> findAllByCategoryIdWithBannerPriceMax(@Param("category_id") Integer categoryId);
}
