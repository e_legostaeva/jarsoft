package ru.jarsoft.legostaeva.testovoe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.jarsoft.legostaeva.testovoe.model.entity.Request;

@Repository
public interface RequestRepository extends JpaRepository<Request, Integer> {
}
