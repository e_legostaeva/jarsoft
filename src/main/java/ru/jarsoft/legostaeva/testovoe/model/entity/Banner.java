package ru.jarsoft.legostaeva.testovoe.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "banner")
@Accessors(chain = true)
public class Banner {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer bannerId;

    @NotNull
    @Column(name = "name")
    private String bannerName;

    @NotNull
    @Column(name = "price")
    @Min(0)
    private BigDecimal bannerPrice;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    private Category category;

    @NotNull
    @Column(name = "content", columnDefinition = "text")
    private String bannerContent;

    @Column(name = "deleted")
    private Boolean bannerIsDeleted;

    @OneToMany(mappedBy = "banner")
    private Set<Request> requests = new HashSet<>();
}
