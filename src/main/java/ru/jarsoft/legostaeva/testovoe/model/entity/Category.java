package ru.jarsoft.legostaeva.testovoe.model.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.annotation.Nonnull;
import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "category")
@Accessors(chain = true)
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer categoryId;

    @Nonnull
    @Column(name = "name")
    private String categoryName;

    @Nonnull
    @Column(name = "req_name")
    private String categoryReqName;

    @Column(name = "deleted")
    private Boolean categoryIsDeleted;

    @OneToMany(mappedBy = "category")
    private Set<Banner> banners = new HashSet<>();
}
