package ru.jarsoft.legostaeva.testovoe.model.dto.category;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class CategoryDetailDto {
    private Integer categoryId;
    private String categoryName;
    private String categoryReqName;
}
