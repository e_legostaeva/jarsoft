package ru.jarsoft.legostaeva.testovoe.model.dto.category;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CategoryGridDto {
    private Integer categoryId;
    private String categoryName;
}
