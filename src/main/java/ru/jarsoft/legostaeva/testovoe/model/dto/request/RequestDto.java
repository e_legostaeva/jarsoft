package ru.jarsoft.legostaeva.testovoe.model.dto.request;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.BannerDetailDto;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestDto {
    private Integer requestId;
    private BannerDetailDto bannerDto;
    private String requestUserAgent;
    private String requestIpAddress;
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate requestDate;
}
