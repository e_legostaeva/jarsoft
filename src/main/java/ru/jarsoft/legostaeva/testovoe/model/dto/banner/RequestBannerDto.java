package ru.jarsoft.legostaeva.testovoe.model.dto.banner;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RequestBannerDto {
    private Integer bannerId;
    private String bannerContent;
}
