package ru.jarsoft.legostaeva.testovoe.model.dto.banner;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateBannerDto {
    private Integer bannerId;
    private String bannerName;
    private BigDecimal bannerPrice;
    private Integer categoryId;
    private String bannerContent;
}
