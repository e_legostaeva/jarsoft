package ru.jarsoft.legostaeva.testovoe.model.dto.banner;

import lombok.*;
import ru.jarsoft.legostaeva.testovoe.model.dto.category.CategoryGridDto;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BannerDetailDto {
    private Integer bannerId;
    private String bannerName;
    private BigDecimal bannerPrice;
    private CategoryGridDto categoryDto;
    private String bannerContent;
    private Boolean bannerIsDeleted;
}
