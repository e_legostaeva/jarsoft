package ru.jarsoft.legostaeva.testovoe.converter;

import org.springframework.stereotype.Component;
import ru.jarsoft.legostaeva.testovoe.model.dto.category.CategoryDetailDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.category.CategoryGridDto;
import ru.jarsoft.legostaeva.testovoe.model.entity.Category;

import javax.annotation.Nonnull;

@Component
public class CategoryConverter {
    @Nonnull
    public CategoryDetailDto fromEntity(Category source) {
        return CategoryDetailDto.builder()
                .categoryId(source.getCategoryId())
                .categoryName(source.getCategoryName())
                .categoryReqName(source.getCategoryReqName())
                .build();
    }

    @Nonnull
    public Category fromDto(CategoryDetailDto source) {
        return new Category()
                .setCategoryId(source.getCategoryId())
                .setCategoryName(source.getCategoryName())
                .setCategoryReqName(source.getCategoryReqName())
                .setCategoryIsDeleted(false);
    }

    @Nonnull
    public CategoryGridDto fromEntityToGridDto(Category source) {
        return CategoryGridDto.builder()
                .categoryId(source.getCategoryId())
                .categoryName(source.getCategoryName())
                .build();
    }
}
