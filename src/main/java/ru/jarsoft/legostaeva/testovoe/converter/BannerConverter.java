package ru.jarsoft.legostaeva.testovoe.converter;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.BannerDetailDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.BannerGridDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.CreateBannerDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.RequestBannerDto;
import ru.jarsoft.legostaeva.testovoe.model.entity.Banner;
import ru.jarsoft.legostaeva.testovoe.model.entity.Category;

import javax.annotation.Nonnull;

@Component
@RequiredArgsConstructor
public class BannerConverter {
    private final CategoryConverter categoryConverter;

    @Nonnull
    public BannerDetailDto fromEntity(Banner source) {
        return BannerDetailDto.builder()
                .bannerId(source.getBannerId())
                .bannerName(source.getBannerName())
                .bannerPrice(source.getBannerPrice())
                .categoryDto(categoryConverter.fromEntityToGridDto(source.getCategory()))
                .bannerContent(source.getBannerContent())
                .bannerIsDeleted(source.getBannerIsDeleted())
                .build();
    }

    @Nonnull
    public BannerGridDto fromEntityToGridDto(Banner source) {
        return BannerGridDto.builder()
                .bannerId(source.getBannerId())
                .bannerName(source.getBannerName())
                .build();
    }

    @Nonnull
    public Banner fromCreateDtoToEntity(CreateBannerDto source, Category category) {
        return new Banner()
                .setBannerName(source.getBannerName())
                .setBannerPrice(source.getBannerPrice())
                .setCategory(category)
                .setBannerContent(source.getBannerContent())
                .setBannerIsDeleted(false);
    }

    @Nonnull
    public RequestBannerDto fromEntityToRequestBannerDto(Banner source) {
        return RequestBannerDto.builder()
                .bannerId(source.getBannerId())
                .bannerContent(source.getBannerContent())
                .build();
    }
}
