package ru.jarsoft.legostaeva.testovoe.exception;

public class NoContentException extends RuntimeException {
    public NoContentException(String message) {
        super(message);
    }
}
