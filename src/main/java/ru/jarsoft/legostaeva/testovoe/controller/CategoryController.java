package ru.jarsoft.legostaeva.testovoe.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.jarsoft.legostaeva.testovoe.model.dto.category.CategoryDetailDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.category.CategoryGridDto;
import ru.jarsoft.legostaeva.testovoe.service.CategoryService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class CategoryController {
    private final CategoryService categoryService;

    @GetMapping("/category")
    public List<CategoryGridDto> getAllCategories() {
        return categoryService.getAllCategories();
    }

    @PostMapping("/category")
    public CategoryDetailDto newCategory(@RequestBody CategoryDetailDto categoryDto) {
        return categoryService.saveCategory(categoryDto);
    }

    @GetMapping("/category/{id}")
    public CategoryDetailDto retrieveCategory(@PathVariable Integer id) {
        return categoryService.getById(id);
    }

    @DeleteMapping("/category/{id}")
    public void deleteCategory(@PathVariable Integer id) {
        categoryService.deleteById(id);
    }

    @PutMapping("/category")
    public CategoryDetailDto updateCategory(@RequestBody CategoryDetailDto categoryDto) {
        return categoryService.updateCategory(categoryDto);
    }

    @GetMapping("/category/search/{searchText}")
    public List<CategoryGridDto> findAllBySearchText(@PathVariable String searchText) {
        return categoryService.findAllBySearchText(searchText);
    }
}
