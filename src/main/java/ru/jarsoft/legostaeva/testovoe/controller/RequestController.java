package ru.jarsoft.legostaeva.testovoe.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.RequestBannerDto;
import ru.jarsoft.legostaeva.testovoe.service.BannerService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class RequestController {
    private final BannerService bannerService;

    @GetMapping("/bid")
    public RequestBannerDto findBannerByCategoryReqName(
            @RequestHeader("User-Agent") String userAgent,
            @RequestParam(name = "category") String categoryReqName,
            HttpServletRequest request
    ) {
        System.out.println(userAgent);
        System.out.println(request.getRemoteAddr());
        return bannerService.findBannerByCategoryReqName(
                categoryReqName,
                userAgent,
                request.getRemoteAddr()
        );
    }

}
