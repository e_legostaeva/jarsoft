package ru.jarsoft.legostaeva.testovoe.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.BannerDetailDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.BannerGridDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.CreateBannerDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.UpdateBannerDto;
import ru.jarsoft.legostaeva.testovoe.service.BannerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:3000")
public class BannerController {
    private final BannerService bannerService;

    @GetMapping("/banner")
    public List<BannerGridDto> getAllBanners() {
        return bannerService.getAllBanners();
    }

    @PostMapping("/banner")
    public BannerDetailDto newBanner(@Valid @RequestBody CreateBannerDto bannerDto) {
        return bannerService.saveBanner(bannerDto);
    }

    @GetMapping("/banner/{id}")
    public BannerDetailDto retrieveBanner(@PathVariable Integer id) {
        return bannerService.getById(id);
    }

    @DeleteMapping("/banner/{id}")
    public void deleteBanner(@PathVariable Integer id) {
        bannerService.deleteById(id);
    }

    @PutMapping("/banner")
    public BannerDetailDto updateBanner(@RequestBody UpdateBannerDto bannerDto) {
        return bannerService.updateBanner(bannerDto);
    }

    @GetMapping("/banner/search/{searchText}")
    public List<BannerGridDto> findAllBySearchText(@PathVariable String searchText) {
        return bannerService.findAllBySearchText(searchText);
    }
}
