package ru.jarsoft.legostaeva.testovoe.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jarsoft.legostaeva.testovoe.converter.CategoryConverter;
import ru.jarsoft.legostaeva.testovoe.exception.BadRequestException;
import ru.jarsoft.legostaeva.testovoe.exception.NotFoundException;
import ru.jarsoft.legostaeva.testovoe.model.dto.category.CategoryDetailDto;
import ru.jarsoft.legostaeva.testovoe.model.dto.category.CategoryGridDto;
import ru.jarsoft.legostaeva.testovoe.model.entity.Banner;
import ru.jarsoft.legostaeva.testovoe.model.entity.Category;
import ru.jarsoft.legostaeva.testovoe.repository.CategoryRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryRepository categoryRepository;
    private final CategoryConverter categoryConverter;

    public List<CategoryGridDto> getAllCategories() {
        return categoryRepository.findAllByCategoryIsDeleted(false)
                .stream()
                .map(categoryConverter::fromEntityToGridDto)
                .collect(Collectors.toList());
    }

    public CategoryDetailDto saveCategory(CategoryDetailDto categoryDto) {
        String categoryName = categoryDto.getCategoryName();
        if (!categoryNameIsValid(categoryName)) {
            throw new BadRequestException("Category with name '" + categoryName + "' is already exist");
        }

        String categoryReqName = categoryDto.getCategoryReqName();
        if (!categoryReqNameIsValid(categoryReqName)) {
            throw new BadRequestException("Category with required name '" + categoryReqName + "' is already exist");
        }

        return categoryConverter.fromEntity(
                categoryRepository.save(categoryConverter.fromDto(categoryDto))
        );
    }

    public CategoryDetailDto getById(Integer id) {
        return categoryConverter.fromEntity(getEntityById(id));
    }

    public void deleteById(Integer id) {
        Category category = getEntityById(id);
        List<Banner> categoryBanners = category.getBanners().stream()
                .filter(banner -> banner.getBannerIsDeleted() != null)
                .filter(banner -> !banner.getBannerIsDeleted())
                .collect(Collectors.toList());
        if (categoryBanners.isEmpty()) {
            category.setCategoryIsDeleted(null);
        } else {
            List<Integer> bannerIds = categoryBanners.stream()
                    .map(Banner::getBannerId)
                    .collect(Collectors.toList());
            throw new BadRequestException(
                    String.format(
                            "Cannot delete category. Related banners IDs: %s.",
                            bannerIds
                    )
            );
        }
    }

    public CategoryDetailDto updateCategory(CategoryDetailDto newCategory) {
        String categoryName = newCategory.getCategoryName();
        Integer categoryWithSameNameId = categoryRepository.findCategoryIdWithNameLike(categoryName);
        if ((categoryWithSameNameId != null) && !newCategory.getCategoryId().equals(categoryWithSameNameId)) {
            throw new BadRequestException("Category with name '" + categoryName + "' is already exist");
        }

        String categoryReqName = newCategory.getCategoryReqName();
        Integer categoryWithSameReqNameId = categoryRepository.findCategoryIdWithReqNameLike(categoryReqName);
        if ((categoryWithSameReqNameId != null) && !newCategory.getCategoryId().equals(categoryWithSameReqNameId)) {
            throw new BadRequestException("Category with required name '" + categoryReqName + "' is already exist");
        }

        return categoryConverter.fromEntity(
                getEntityById(newCategory.getCategoryId())
                        .setCategoryName(newCategory.getCategoryName())
                        .setCategoryReqName(newCategory.getCategoryReqName())
        );
    }

    public Category getEntityById(Integer id) {
        return Optional.ofNullable(categoryRepository.findCategoryByCategoryId(id))
                .orElseThrow(() -> new NotFoundException("Can't find category"));
    }

    public List<CategoryGridDto> findAllBySearchText(String searchText) {
        return categoryRepository.findAllByCategoryNameLikeAndCategoryIsDeletedFalse("%" + searchText + "%")
                .stream()
                .map(categoryConverter::fromEntityToGridDto)
                .collect(Collectors.toList());
    }

    private Boolean categoryNameIsValid(String name) {
        return categoryRepository.findCategoryIdWithNameLike(name) == null;
    }

    private Boolean categoryReqNameIsValid(String name) {
        return categoryRepository.findCategoryIdWithReqNameLike(name) == null;
    }

    public Optional<Category> findCategoryByCategoryReqName(String categoryReqName) {
        return categoryRepository.findCategoryByCategoryReqNameAndCategoryIsDeletedFalse(categoryReqName);
    }
}
