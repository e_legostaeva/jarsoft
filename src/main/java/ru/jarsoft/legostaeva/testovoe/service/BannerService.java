package ru.jarsoft.legostaeva.testovoe.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.jarsoft.legostaeva.testovoe.converter.BannerConverter;
import ru.jarsoft.legostaeva.testovoe.exception.BadRequestException;
import ru.jarsoft.legostaeva.testovoe.exception.NoContentException;
import ru.jarsoft.legostaeva.testovoe.exception.NotFoundException;
import ru.jarsoft.legostaeva.testovoe.model.dto.banner.*;
import ru.jarsoft.legostaeva.testovoe.model.entity.Banner;
import ru.jarsoft.legostaeva.testovoe.model.entity.Category;
import ru.jarsoft.legostaeva.testovoe.model.entity.Request;
import ru.jarsoft.legostaeva.testovoe.repository.BannerRepository;
import ru.jarsoft.legostaeva.testovoe.repository.RequestRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class BannerService {
    private final BannerRepository bannerRepository;
    private final RequestRepository requestRepository;
    private final BannerConverter bannerConverter;
    private final CategoryService categoryService;
    private final Random random = new Random();

    public List<BannerGridDto> getAllBanners() {
        return bannerRepository.findAllByBannerIsDeleted(false)
                .stream()
                .map(bannerConverter::fromEntityToGridDto)
                .collect(Collectors.toList());
    }

    public BannerDetailDto saveBanner(CreateBannerDto bannerDto) {
        String bannerName = bannerDto.getBannerName();
        Integer bannerWithSameNameId = bannerRepository.findBannerIdWithNameLike(bannerName);
        if ((bannerWithSameNameId != null) && !bannerDto.getBannerId().equals(bannerWithSameNameId)) {
            throw new BadRequestException("Banner with name '" + bannerName + "' is already exist");
        }

        Category category = categoryService.getEntityById(bannerDto.getCategoryId());
        return bannerConverter.fromEntity(
                bannerRepository.save(bannerConverter.fromCreateDtoToEntity(bannerDto, category))
        );
    }

    public BannerDetailDto getById(Integer id) {
        return bannerConverter.fromEntity(getEntityById(id));
    }

    private Banner getEntityById(Integer id) {
        return Optional.ofNullable(bannerRepository.findBannerByBannerId(id))
                .orElseThrow(() -> new NotFoundException("Can't find banner"));
    }

    public void deleteById(Integer id) {
        getEntityById(id).setBannerIsDeleted(null);
    }

    public BannerDetailDto updateBanner(UpdateBannerDto newBanner) {
        String bannerName = newBanner.getBannerName();
        if (!bannerNameIsValid(bannerName)) {
            throw new BadRequestException("Banner with name '" + bannerName + "' is already exist");
        }

        return bannerConverter.fromEntity(
                getEntityById(newBanner.getBannerId())
                        .setBannerName(newBanner.getBannerName())
                        .setBannerPrice(newBanner.getBannerPrice())
                        .setCategory(categoryService.getEntityById(newBanner.getCategoryId()))
                        .setBannerContent(newBanner.getBannerContent())
        );
    }

    public List<BannerGridDto> findAllBySearchText(String searchText) {
        return bannerRepository.findAllByBannerNameLikeAndBannerIsDeletedFalse("%" + searchText + "%")
                .stream()
                .map(bannerConverter::fromEntityToGridDto)
                .collect(Collectors.toList());
    }

    private Boolean bannerNameIsValid(String name) {
        return bannerRepository.findBannerIdWithNameLike(name) == null;
    }

    public RequestBannerDto findBannerByCategoryReqName(String categoryReqName, String userAgent, String ipAddress) {
        Optional<Category> categoryOpt = categoryService.findCategoryByCategoryReqName(categoryReqName);
        if (!categoryOpt.isPresent()) {
            throw new NotFoundException(String.format("Category with name %s doesn't exist.", categoryReqName));
        }

        Category category = categoryOpt.get();
        LocalDateTime requestDate = LocalDateTime.now();
        List<Integer> categoryBannersIds = bannerRepository.findAllAvailableBanners(
                category.getCategoryId(),
                ipAddress,
                userAgent,
                requestDate
        );

        if (categoryBannersIds.isEmpty()) {
            throw new NoContentException(
                    String.format(
                            "Cannot find any available banner in category with required name '%s'.",
                            categoryReqName
                    )
            );
        }

        Banner banner;
        if (categoryBannersIds.size() > 1) {
            banner = chooseBanner(category.getCategoryId());
        } else {
            banner = getEntityById(categoryBannersIds.get(0));
        }

        requestRepository.save(
                new Request()
                        .setBanner(banner)
                        .setRequestIpAddress(ipAddress)
                        .setRequestUserAgent(userAgent)
                        .setRequestDate(requestDate)
        );
        return bannerConverter.fromEntityToRequestBannerDto(banner);
    }

    private Banner chooseBanner(Integer categoryId) {
        List<Integer> bannersWithMaxPrice = bannerRepository.findAllByCategoryIdWithBannerPriceMax(categoryId);
        int elementIndex = random.nextInt(bannersWithMaxPrice.size());
        return getEntityById(bannersWithMaxPrice.get(elementIndex));
    }
}
