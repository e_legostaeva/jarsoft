create table category
(
	category_id integer auto_increment unique,
	name varchar(255) not null,
	req_name varchar(255) not null,
	deleted boolean,
	unique (name, deleted),
	unique (req_name, deleted),
	constraint category_pk
		primary key (category_id)
);

create table banner
(
	banner_id integer auto_increment unique,
	name varchar(255) not null,
	price decimal(8,2) not null,
	category_id integer not null,
	content text not null,
	deleted boolean,
	unique (name, deleted),
	constraint banner_pk
		primary key (banner_id),
	foreign key (category_id)
	  references category (category_id)
);

create table request
(
	request_id integer auto_increment unique,
	banner_id integer not null,
	user_agent text not null,
	ip_address varchar(255) not null,
	date datetime not null,
	constraint request_pk
		primary key (request_id),
	foreign key (banner_id)
	  references banner (banner_id)
);